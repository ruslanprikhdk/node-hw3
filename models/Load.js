const {Schema, model} = require('mongoose');

const Load = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String, required: true},  
  status: {type: String, required: true},
  state: {type: String, required: true},
  name: {type: String, required: true},
  payload: {type: Number, required: true},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: {type: Object, required: true},
  logs: {type: Array, required: false}, 
  createdDate: {type: String, required: true}
})

module.exports = model('Load', Load);