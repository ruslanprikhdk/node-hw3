const Router = require('express');
const router = new Router();
const controller = require('../controller');
const {check} = require('express-validator');
const authMiddleware = require('../middleware/auth.middleware');

router.post('/auth/register', [
  check('email', 'Incorrect email').isEmail()
], controller.registration);
router.post('/auth/login', controller.login);
router.post('/auth/forgot_password', controller.forgot_password);
router.get('/users/me', authMiddleware, controller.getDriverInfo);
router.delete('/users/me', authMiddleware, controller.deleteDriver);
router.patch('/users/me/password', authMiddleware, controller.changePassword);
router.get('/trucks', authMiddleware, controller.getAllTrucks);
router.post('/trucks', authMiddleware, controller.addNewTruck);
router.get('/trucks/:id', authMiddleware, controller.getTruckById);
router.put('/trucks/:id', authMiddleware, controller.modifyTruckById);
router.delete('/trucks/:id', authMiddleware, controller.deleteTruckById);
router.post('/trucks/:id/assign', authMiddleware, controller.assignTruckById);
router.get('/loads', authMiddleware, controller.getAllLoads);
router.post('/loads', authMiddleware, controller.addNewLoad);
router.get('/loads/active', authMiddleware, controller.getActiveLoad);
router.patch('/loads/active/state', authMiddleware, controller.moveLoadState);
router.get('/loads/:id', authMiddleware, controller.getLoadById);
router.put('/loads/:id', authMiddleware, controller.modifyLoadById);
router.delete('/loads/:id', authMiddleware, controller.deleteLoadById);
router.post('/loads/:id/post', authMiddleware, controller.postLoadById);
router.get('/loads/:id/shipping_info', authMiddleware, controller.getShippingInfoById);

module.exports = router;