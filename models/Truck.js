const {Schema, model} = require('mongoose');

const Truck = new Schema({
  created_by: {type: String, required: true},
  assigned_to: {type: String, required: true},
  type: {type: String, required: true},
  status: {type: String, required: true},
  createdDate: {type: String, required: true}
})

module.exports = model('Truck', Truck);