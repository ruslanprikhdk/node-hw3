const User = require('./models/User');
const Load = require('./models/Load');
const Truck = require('./models/Truck');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const config = require('config');
const crypto = require('crypto');
const sendEmail = require('./email');

const generateAccessToken = (id, role) => {
  const payload = {
    id, 
    role
  }
  return jwt.sign(payload, config.get('secretKey'), {expiresIn: '24h'});
}

const createPasswordResetToken = () => {
  const resetToken = crypto.randomBytes(32).toString('hex');
  crypto.createHash('sha256').update(resetToken).digest('hex');
  return resetToken;
}

function getStringBetween(str, start, end) {
  const result = str.match(new RegExp(start + "(.*)" + end));
  return result[1];
}

class Controller {

  async registration (req, res) {
    try {
      const errors = validationResult(req);
      if(!errors.isEmpty()) {
        return res.status(400).send({message:'Input is not valid'});
      }
      const {email, password, role} = req.body;
      if (role.toLowerCase() !== 'driver' && role.toLowerCase() !== 'shipper') {
        return res.status(400).send({message:'Please, provide correct role'});
      } 
      const candidate = await User.findOne({email});
        if (candidate) {
          return res.status(400).send({message:'User already exists'});
        } 
      const createdDate = new Date();
      const protectedPassword = await bcrypt.hash(password, 10);
      const user = new User({role: role.toUpperCase(), email, password: protectedPassword, createdDate: createdDate.toISOString()});
      await user.save();
      return res.status(200).send({message:'Profile created successfully'});

    } catch {
      res.status(400).send({message:'Please, check your input'});
    }
  }

  async login (req, res) {
    try {
      const {email, password} = req.body;
      const user = await User.findOne({email});
      if(!user) {
        return res.status(400).send({message: `User with email ${email} is not found`});
      }
      const validPassword = bcrypt.compareSync(password, user.password);
      if(!validPassword) {
        return res.status(400).send({message: 'Please, check your password'});
      }
      const token = generateAccessToken(user._id, user.role);
      return res.status(200).send({jwt_token: token});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }
  }

  async forgot_password (req, res) {
    try {
      const {email} = req.body;
      const user = await User.findOne({email});
      if(!user) {
        return res.status(400).send({message: `User with email ${email} is not found`});
      }
      const newToken = createPasswordResetToken();
      console.log(newToken);
      sendEmail({email: email,subject:'Hi, here is your new password',message: newToken})
      return res.status(200).send({jwt_token: newToken});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }
  }

  async getDriverInfo (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      res.status(200).send({user:{_id: user._id, role: user.role, email: user.email, createdDate: user.createdDate}});
    } catch {
      res.status(400).send({message: 'Please, check whether user exists'});
    }  
  }

  async deleteDriver (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);     
      user.remove();
      res.status(200).send({message: 'Profile deleted successfully'});               
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }

  async changePassword (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);     
      const {newPassword} = req.body;
      const newProtectedPassword = await bcrypt.hash(newPassword, 10);
      user.password = newProtectedPassword;
      user.save();
      res.status(200).send({message: 'Password changed successfully'});               
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }

  async getAllTrucks (req, res) {
    try {  
      const trucks = await Truck.find();
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot view trucks'});
      }
      res.status(200).send({trucks: trucks});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }

  async addNewTruck (req, res) {
    try {    
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot create trucks'});
      }
      const allowedTruckTypes = ['sprinter','small straight', 'large straight'];
      const {type} = req.body;
      if(!allowedTruckTypes.includes(type.toLowerCase())) {
        return res.status(400).send({message:'Please, provide correct truck type'});
      }
      const createdDate = new Date();
      const truck = new Truck({created_by: user._id, assigned_to: "NA", type: type.toUpperCase(), status: "IS", createdDate: createdDate.toISOString()});
      await truck.save();
      return res.status(200).send({message:'Truck was successfully created'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }  
  }

  async getTruckById (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot view trucks'});
      }
      const truckId = req.path.substring(8);
      const truck = await Truck.findById(truckId);
      res.status(200).send({truck:{_id: truck._id, created_by: truck.created_by, assigned_to: truck.assigned_to, type: truck.type, status: truck.status, createdDate: truck.createdDate}});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }  
  }

  async modifyTruckById (req, res) {
    try {
      const {type} = req.body;
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot modify trucks'});
      }
      const truckId = req.path.substring(8);
      const truck = await Truck.findById(truckId);
      truck.type = type;
      truck.save();
      res.status(200).send({message: 'Truck details changed successfully'});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }  
  }

  async deleteTruckById (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot delete trucks'});
      }
      const truckId = req.path.substring(8);
      const truck = await Truck.findById(truckId);     
      truck.remove();
      res.status(200).send({message: 'Truck deleted successfully'});
                      
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    } 
  }

  async assignTruckById (req, res) {
    try {
      let driverIsAssigned = false;
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot modify trucks'});
      }
      const truckId = getStringBetween(req.path,'trucks/','/assign');
      const truck = await Truck.findById(truckId);
      if (truck.assigned_to !== "NA") {
        return res.status(400).send({message: 'This truck already has driver assigned'});
      }
      const trucks = await Truck.find();
      trucks.forEach(truck => {
        if(truck.assigned_to === String(user._id)) {
          driverIsAssigned = true;
        }
      })
      if(driverIsAssigned) {
        return res.status(400).send({message: 'This driver already has truck assigned'});
      }
      truck.assigned_to = user._id;
      truck.save();
      res.status(200).send({message: 'Truck assigned successfully'});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  }

  async getAllLoads (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      let offset = 0;
      let limit = 10;
      let statusQuery;
      if (req.query.status) {
        statusQuery = req.query.status.toUpperCase();
      };
      if (req.query.offset) {
        offset = req.query.offset;
      };
      if (req.query.limit) {
        if(req.query.limit > 50) {
          limit = 50;
        } else {
          limit = req.query.limit;
        }
      };
      const loads = await Load.find();
      if(user.role === "SHIPPER") {
        const activeLoads = loads.filter(load => load.created_by === String(user._id));
        let filteredActiveLoads;
        if (statusQuery) {
          filteredActiveLoads = activeLoads.filter(load => load.status === statusQuery);
        } else {
          filteredActiveLoads = activeLoads;
        }
        const slicedFilteredActiveLoads = filteredActiveLoads.slice(offset);
        const limitedSlicedFilteredActiveLoads = slicedFilteredActiveLoads.slice(0,limit);
        return res.status(200).send({loads: limitedSlicedFilteredActiveLoads});
      } else {
        const activeLoads = loads.filter(load => load.assigned_to === String(user._id));
        let filteredActiveLoads;
        if (statusQuery) {
          filteredActiveLoads = activeLoads.filter(load => load.status === statusQuery);
        } else {
          filteredActiveLoads = activeLoads;
        }
        const slicedFilteredActiveLoads = filteredActiveLoads.slice(offset);
        const limitedSlicedFilteredActiveLoads = slicedFilteredActiveLoads.slice(0,limit);
        return res.status(200).send({loads: limitedSlicedFilteredActiveLoads});
      }

    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }  
  }

  async addNewLoad (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "DRIVER") {
        return res.status(400).send({message:'Drivers cannot create loads'});
      }
      const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
      const createdDate = new Date();
      const load = new Load({created_by: user._id, assigned_to: "NA", status: "NEW", state: "NA", name, payload, pickup_address, delivery_address, dimensions, createdDate: createdDate.toISOString()});
      load.save();
      return res.status(200).send({message:'Load created successfully'});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }  
  }

  async getActiveLoad (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot view active loads'});
      }
      const loads = await Load.find();
      const activeLoad = loads.filter(load => load.status === "ASSIGNED").filter(load => load.assigned_to === String(user._id));
      return res.status(200).send({load: activeLoad[0]});
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }  
  }

  async moveLoadState (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "SHIPPER") {
        return res.status(400).send({message:'Shippers cannot change load state'});
      }
      const trucks = await Truck.find();
      const assignedTruck = trucks.filter(truck => truck.assigned_to === String(user._id))[0];
      const loads = await Load.find();
      const activeLoad = loads.filter(load => load.status === "ASSIGNED").filter(load => load.assigned_to === String(user._id))[0];
      if(!activeLoad) {
        return res.status(400).send({message:'Driver does not have active loads'});
      }
      switch (activeLoad.state) {
        case `En route to Pick Up`:
          activeLoad.state = "Arrived to Pick Up";
          activeLoad.save();
          res.status(200).send({message:'Load state changed to Arrived to Pick Up'});
          break;
        case "Arrived to Pick Up":
          activeLoad.state = "En route to delivery";
          activeLoad.save();
          res.status(200).send({message:'Load state changed to En route to delivery'});
          break;
        case "En route to delivery":
          activeLoad.state = "Arrived to delivery";
          activeLoad.status = "SHIPPED"
          activeLoad.save();
          assignedTruck.status = "IS";
          assignedTruck.save();
          res.status(200).send({message:'Load state changed to Arrived to delivery'});
          break;
        default:
          return res.status(400).send({message:'Please, check your input'});
        }
    } catch {
      res.status(400).send({message:'Please, check your input'});
    }  
  }

  async getLoadById (req, res) {
    try {
      const loadId = req.path.substring(7);
      const load = await Load.findById(loadId);
      res.status(200).send({load:load});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }  
  }

  async modifyLoadById (req, res) {
    try {    
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "DRIVER") {
        return res.status(400).send({message:'Drivers cannot modify loads'});
      }
      const loadId = req.path.substring(7);
      const load = await Load.findById(loadId);
      const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
      if (!name || !payload || !pickup_address || !delivery_address || !dimensions) {
        res.status(400).send({message: 'Please, check your input'});  
      }
      load.name = name;
      load.payload = payload;
      load.pickup_address = pickup_address;
      load.delivery_address = delivery_address;
      load.dimensions = dimensions;
      load.save();
      res.status(200).send({message: 'Load details changed successfully'});
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }  
  }

  async deleteLoadById (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "DRIVER") {
        return res.status(400).send({message:'Drivers cannot delete loads'});
      }
      const loadId = req.path.substring(7);
      const load = await Load.findById(loadId);     
      load.remove();
      res.status(200).send({message: 'Load deleted successfully'});
                      
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    } 
  }

  async postLoadById (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "DRIVER") {
        return res.status(400).send({message:'Drivers cannot post loads'});
      }
      const loadId = getStringBetween(req.path,'loads/','/post');
      const load = await Load.findById(loadId);
      if (load.status !== "NEW") { 
        return res.status(400).send({message:'This load has already been posted'});
      }
      if (load.created_by !== String(user._id)) {
        return res.status(400).send({message:'This is a load created by another shipper'});
      }
      load.status = "POSTED";
      const trucks = await Truck.find();
      const filteredTrucks = trucks.filter(truck => truck.assigned_to !== "NA").filter(truck => truck.status === "IS");
      const truckFound = checkTruck(filteredTrucks,[load.dimensions.width, load.dimensions.length, load.dimensions.height, load.payload]);
      if (truckFound) {
        truckFound.status = "OL";
        truckFound.save();
        load.status = "ASSIGNED";
        load.assigned_to = truckFound.assigned_to;
        load.state = "En route to Pick Up";
        load.logs.push("Driver was found and assigned");
        load.save();
        res.status(200).send({message: 'Load posted successfully', driver_found: true});
      } else {
        load.status = "NEW";
        load.logs.push("Driver was not found");
        load.save();
        res.status(200).send({message: 'Load posted successfully', driver_found: false});
      }
      
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  } 

  async getShippingInfoById (req, res) {
    try {
      const userCreds = req.user;
      const user = await User.findById(userCreds.id);
      if(user.role === "DRIVER") {
        return res.status(400).send({message:'Drivers cannot receive shipping_info'});
      }
      const loadId = getStringBetween(req.path,'loads/','/shipping_info');
      const load = await Load.findById(loadId);
      if (load.status !== "ASSIGNED") {
        return res.status(400).send({message: 'This is not active load'});
      }
      const trucks = await Truck.find();
      const truck = trucks.filter(truck => truck.assigned_to === load.assigned_to);
      res.status(200).send({load: load, truck: truck});      
    } catch {
      res.status(400).send({message: 'Please, check your input'});
    }   
  } 

}

function checkTruck(trucksArr, loadDims) {
  let requiredTruck;
  const truckDimensions = {
    'SPRINTER': [300, 250, 170, 1700],
    'SMALL STRAIGHT': [500, 250, 170, 2500],
    'LARGE STRAIGHT': [700, 350, 200 ,4000]
  }
  for (let i = 0; i < trucksArr.length; i++) {
    let truck = trucksArr[i];
    let truckStat = truckDimensions[truck.type];
    if (truckStat[0] > loadDims[0] && truckStat[1] > loadDims[1] && truckStat[2] > loadDims[2] && truckStat[3] > loadDims[3]) {
      requiredTruck = truck;
      break;
    }
  }
  return requiredTruck;
}

module.exports = new Controller();

