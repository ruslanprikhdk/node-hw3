const {Schema, model} = require('mongoose');

const User = new Schema({ 
  role: {type: String, required: true},
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},  
  createdDate: {type: String, required: true}
})

module.exports = model('User', User);