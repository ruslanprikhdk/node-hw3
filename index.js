const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const morgan = require('morgan');
const router = require('./routes/router');
const PORT = process.env.PORT ||config.get('serverPort');
const cors = require('cors');
const app = express();


app.use(express.json());
app.use(cors());
app.use(morgan('combined'));
// app.use('/api/auth', router);
app.use('/api', router);

const start = async () => {
  try {

    await mongoose.connect(config.get('dbUrl'));

    app.listen(PORT, () => {
      console.log('Server started on port ', PORT);
    });
  } catch (e) {

  }
}

start();