const nodemailer = require('nodemailer');
const config = require('config');

const sendEmail = async options => {
  const transporter = nodemailer.createTransport({
    host: config.get("email_host"),
    port: config.get("email_port"),
    auth: {
      user: config.get("email_username"),
      pass: config.get("email_password")
    }
  });

  const mailOptions = {
    from: 'Ruslan Prykhodko <ruslan@somemail.com>',
    to: options.email,
    subject: options.subject,
    text: options.message
  }
  await transporter.sendMail(mailOptions);
}

module.exports = sendEmail;